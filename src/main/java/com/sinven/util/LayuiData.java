package com.sinven.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
/*@AllArgsConstructor*/
public class LayuiData<T> {

    String msg;//消息
    String code;//编码 200 404 500 415
    Integer count;//总数据条数
    List<T> data;//集合数据


    public LayuiData(String msg, String code, Integer count, List<T> data) {
        this.msg = msg;
        this.code = code;
        this.count = count;
        this.data = data;
    }
}
