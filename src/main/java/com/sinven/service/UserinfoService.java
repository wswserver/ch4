package com.sinven.service;

import com.sinven.pojo.Userinfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserinfoService {

    public int add(Userinfo c);

    public int delete(Integer id);

    public int edit(Userinfo c);

    public Userinfo findById(Integer id);

    public List<Userinfo> findAll();

    public int cuscount();

    public List<Userinfo> toPage(@Param("skip") int skip, @Param("size") int size);
}
