package com.sinven.service.impl;

import com.sinven.dao.UserinfoDao;
import com.sinven.pojo.Userinfo;
import com.sinven.service.UserinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserinfoServiceImpl implements UserinfoService {
    @Autowired
    UserinfoDao udao;
    @Override
    public int add(Userinfo c) {
        return udao.add(c);
    }

    @Override
    public int delete(Integer id) {
        return udao.delete(id);
    }

    @Override
    public int edit(Userinfo c) {
        return udao.edit(c);
    }

    @Override
    public Userinfo findById(Integer id) {
        return udao.findById(id);
    }

    @Override
    public List<Userinfo> findAll() {
        return udao.findAll();
    }

    @Override
    public int cuscount() {
        return udao.cuscount();
    }

    @Override
    public List<Userinfo> toPage(int skip, int size) {

        return udao.toPage((skip-1)*size,size);
    }
}
