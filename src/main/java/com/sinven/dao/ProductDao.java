package com.sinven.dao;

import com.sinven.pojo.Product;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ProductDao {
    @Insert("insert into product values(null,#{name},#{price},#{discoprice},#{description},#{categoryid})")
    public int add(Product c);

    @Delete("delete from product where id=#{id}")
    public int delete(Integer id);

    @Update("update product set name=#{name},price=#{price},discoprice=#{discoprice},description=#{description},categoryid=#{categoryid} where id=#{id}")
    public int edit(Product c);

    @Select("select * from product where id=#{id}")
    public Product findById(Integer id);

    @Select("select * from product")
    public List<Product> findAll();

    @Select("select count(*) from product")
    public int cuscount();

    @Select("select * from product order by id desc limit #{skip},#{size}")
    public List<Product> toPage(@Param("skip") int skip, @Param("size") int size);
}
