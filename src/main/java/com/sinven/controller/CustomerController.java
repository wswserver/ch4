package com.sinven.controller;

import com.alibaba.fastjson.JSON;
import com.sinven.pojo.Customer;
import com.sinven.service.CustomerService;
import com.sinven.util.LayuiData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @RequestMapping("/showTable")
    public String showTable(){
        return "showTable";
    }

    @RequestMapping("/cuslistajax")
    @ResponseBody
    public String cuslistajax(@RequestParam Map<String,Object> params){
        Integer page=1;
        Integer limit=4;
        try{
            //page= (Integer) params.get("page");
            //limit= (Integer) params.get("limit");
            page = Integer.parseInt(params.get("page").toString());
            limit = Integer.parseInt(params.get("limit").toString());
        }catch (Exception e){}

        List<Customer> all = customerService.toPage(page,limit);
        int cuscount = customerService.cuscount();
        LayuiData<Customer> lists=new LayuiData<Customer>("","0",cuscount,all);
        String s = JSON.toJSONString(lists);
        System.out.println(s);
        return s;
    }
    @RequestMapping("/addCustomer")
    public String addCustomer(){
        return "addCustomer";
    }

    @PostMapping("/doaddCustomer")
    @ResponseBody
    public String doaddCustomer(@RequestBody Customer customer){
        String msg="成功";
        int add = customerService.add(customer);
        msg=add>0?"添加成功":"添加失败";
        return JSON.toJSONString(msg);
    }

    @RequestMapping("/editcustomer")
    public String editcustomer(Integer id, Model model){
        //通过传递过来的id 查找顾客对象  因为需要在编辑页面显示编辑对象
        Customer cus = customerService.findById(id);
        model.addAttribute("cus",cus);
        return "editCustomer";
    }

    @RequestMapping("/doeditcustomer")
    @ResponseBody
    public String doeditcustomer(@RequestBody Customer cus){
        //通过传递过来的id 查找顾客对象  因为需要在编辑页面显示编辑对象
        int edit = customerService.edit(cus);
        String msg=edit>0?"修改成功":"修改失败";
        return JSON.toJSONString(msg);
    }
    /*删除顾客*/
    @RequestMapping("/dodelcustomer")
    @ResponseBody
    public String dodelcustomer(Integer id){
        //通过传递过来的id 查找顾客对象  因为需要在编辑页面显示编辑对象
        int del = customerService.delete(id);
        String msg=del>0?"1":"2";
        return JSON.toJSONString(msg);
    }

    /*删除多个顾客*/
    @PostMapping("/doalldelcustomer")
    @ResponseBody
    public String doalldelcustomer(@RequestBody List<Customer> customers){
        //通过传递过来的id 查找顾客对象  因为需要在编辑页面显示编辑对象
        int x=0;
        for (Customer c :customers) {
            x+=customerService.delete(c.getId());
        }
        String msg=x>0?"1":"2";
        return JSON.toJSONString(msg);
    }

    @RequestMapping("/doupload")
    @ResponseBody
    public String handlerFormUpload(@RequestParam("name") String name,
                                    @RequestParam("uploadfile") List<MultipartFile> uploadfile,
                                    HttpServletRequest request){
        String msg="";
        if(!uploadfile.isEmpty()&&uploadfile.size()>0){

            for(MultipartFile file:uploadfile){
                //获取文件原始名称
                String oldname=file.getOriginalFilename();
                String dirpath=request.getServletPath()+"/upload";
                System.out.println(dirpath);
                File filepath=new File(dirpath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }
                String newname= UUID.randomUUID()+"-"+oldname;
                try{
                    file.transferTo(new File(dirpath,newname));
                }catch (Exception e){
                    msg=e.getMessage();
                    System.out.println(e.getMessage());
                }
            }
            msg="上传成功";
        }else{
            return "文件不能为空";
        }
        LayuiData<Object> data=new LayuiData<>(msg,"0",0,null);
        return  JSON.toJSONString(data);
    }
}
