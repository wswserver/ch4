package com.sinven.pojo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单类
 * */
public class Orders{
    /** 订单id */
    private String id;

    /** 下单时间 */
    private Date ordertime;

    /** 订单状态 */
    private String orderstatus;

    /** 下单人 */
    private Integer userid;

    /** 总金额 */
    private BigDecimal totalprice;


    /**
     * 获取id
     * 
     * @return id
     */
    public String getId() {
        return this.id;
    }

    /**
     * 设置id
     * 
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取下单时间
     * 
     * @return 下单时间
     */
    public Date getOrdertime() {
        return this.ordertime;
    }

    /**
     * 设置下单时间
     * 
     * @param ordertime
     *          下单时间
     */
    public void setOrdertime(Date ordertime) {
        this.ordertime = ordertime;
    }

    /**
     * 获取订单状态
     * 
     * @return 订单状态
     */
    public String getOrderstatus() {
        return this.orderstatus;
    }

    /**
     * 设置订单状态
     * 
     * @param orderstatus
     *          订单状态
     */
    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }

    /**
     * 获取下单人
     * 
     * @return 下单人
     */
    public Integer getUserid() {
        return this.userid;
    }

    /**
     * 设置下单人
     * 
     * @param userid
     *          下单人
     */
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    /**
     * 获取总金额
     * 
     * @return 总金额
     */
    public BigDecimal getTotalprice() {
        return this.totalprice;
    }

    /**
     * 设置总金额
     * 
     * @param totalprice
     *          总金额
     */
    public void setTotalprice(BigDecimal totalprice) {
        this.totalprice = totalprice;
    }

    public Orders(String id, Date ordertime, String orderstatus, Integer userid, BigDecimal totalprice) {
        this.id = id;
        this.ordertime = ordertime;
        this.orderstatus = orderstatus;
        this.userid = userid;
        this.totalprice = totalprice;
    }

    public Orders() {
    }
}