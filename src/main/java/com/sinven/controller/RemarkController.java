package com.sinven.controller;

import com.sinven.pojo.Admin;
import com.sinven.pojo.Remark;
import com.sinven.service.AdminService;
import com.sinven.service.RemarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class RemarkController {
    @Autowired
    RemarkService remarkService;
    @RequestMapping("/remark")
    public String remarkservice(Model model){
        List<Remark> all = remarkService.findAll();
        model.addAttribute("remarks",all);
        return "/remark/remark";
    }
}
