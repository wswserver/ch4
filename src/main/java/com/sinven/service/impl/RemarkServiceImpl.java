package com.sinven.service.impl;

import com.sinven.dao.RemarkDao;
import com.sinven.pojo.Remark;
import com.sinven.service.RemarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RemarkServiceImpl implements RemarkService {
    @Autowired
    RemarkDao rdao;
    @Override
    public int add(Remark c) {
        return rdao.add(c);
    }

    @Override
    public int delete(Integer id) {
        return rdao.delete(id);
    }

    @Override
    public int edit(Remark c) {
        return rdao.edit(c);
    }

    @Override
    public Remark findById(Integer id) {
        return rdao.findById(id);
    }

    @Override
    public List<Remark> findAll() {
        return rdao.findAll();
    }

    @Override
    public int cuscount() {
        return rdao.cuscount();
    }

    @Override
    public List<Remark> toPage(int skip, int size) {

        return rdao.toPage((skip-1)*size,size);
    }
}
