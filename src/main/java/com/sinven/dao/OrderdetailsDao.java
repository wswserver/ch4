package com.sinven.dao;

import com.sinven.pojo.Orderdetails;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface OrderdetailsDao {
    @Insert("insert into orderdetails values(null,#{productid},#{orderid},#{procount},#{totalprice})")
    public int add(Orderdetails c);

    @Delete("delete from orderdetails where id=#{id}")
    public int delete(Integer id);

    @Update("update orderdetails set productid=#{productid},orderid=#{orderid},procount=#{procount},totalprice=#{totalprice} where id=#{id}")
    public int edit(Orderdetails c);

    @Select("select * from orderdetails where id=#{id}")
    public Orderdetails findById(Integer id);

    @Select("select * from orderdetails")
    public List<Orderdetails> findAll();

    @Select("select count(*) from orderdetails")
    public int cuscount();

    @Select("select * from orderdetails order by id desc limit #{skip},#{size}")
    public List<Orderdetails> toPage(@Param("skip") int skip, @Param("size") int size);
}
