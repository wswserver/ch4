package com.sinven.pojo;

import java.util.Date;


public class Information {
    /**
     * id
     */
    private Integer id;

    /**
     * 管理员
     */
    private Integer adminid;

    /**
     * 公告类容
     */
    private String content;

    /**
     * 公告时间
     */
    private Date infotime;


    /**
     * 获取id
     *
     * @return id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置id
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取管理员
     *
     * @return 管理员
     */
    public Integer getAdminid() {
        return this.adminid;
    }

    /**
     * 设置管理员
     *
     * @param adminid 管理员
     */
    public void setAdminid(Integer adminid) {
        this.adminid = adminid;
    }

    /**
     * 获取公告类容
     *
     * @return 公告类容
     */
    public String getContent() {
        return this.content;
    }

    /**
     * 设置公告类容
     *
     * @param content 公告类容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 获取公告时间
     *
     * @return 公告时间
     */
    public Date getInfotime() {
        return this.infotime;
    }

    /**
     * 设置公告时间
     *
     * @param infotime 公告时间
     */
    public void setInfotime(Date infotime) {
        this.infotime = infotime;
    }

    public Information(Integer id, Integer adminid, String content, Date infotime) {
        this.id = id;
        this.adminid = adminid;
        this.content = content;
        this.infotime = infotime;
    }

    public Information() {
    }
}