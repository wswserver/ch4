package com.sinven.service;

import com.sinven.pojo.Shopcart;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShopcartService {

    public int add(Shopcart c);

    public int delete(Integer id);

    public int edit(Shopcart c);

    public Shopcart findById(Integer id);

    public List<Shopcart> findAll();

    public int cuscount();

    public List<Shopcart> toPage(@Param("skip") int skip, @Param("size") int size);
}
