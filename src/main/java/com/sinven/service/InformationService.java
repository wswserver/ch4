package com.sinven.service;

import com.sinven.pojo.Information;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InformationService {

    public int add(Information c);

    public int delete(Integer id);

    public int edit(Information c);

    public Information findById(Integer id);

    public List<Information> findAll();

    public int cuscount();

    public List<Information> toPage(@Param("skip") int skip, @Param("size") int size);
}
