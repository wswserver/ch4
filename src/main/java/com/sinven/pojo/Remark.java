package com.sinven.pojo;

import java.util.Date;


public class Remark{
    /** id */
    private Integer id;

    /** 评论人 */
    private Integer userid;

    /** 是否显示 */
    private Integer isshow;

    /** 评论类容 */
    private String content;

    /** 评论时间 */
    private Date remarktime;


    /**
     * 获取id
     * 
     * @return id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置id
     * 
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取评论人
     * 
     * @return 评论人
     */
    public Integer getUserid() {
        return this.userid;
    }

    /**
     * 设置评论人
     * 
     * @param userid
     *          评论人
     */
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    /**
     * 获取是否显示
     * 
     * @return 是否显示
     */
    public Integer getIsshow() {
        return this.isshow;
    }

    /**
     * 设置是否显示
     * 
     * @param isshow
     *          是否显示
     */
    public void setIsshow(Integer isshow) {
        this.isshow = isshow;
    }

    /**
     * 获取评论类容
     * 
     * @return 评论类容
     */
    public String getContent() {
        return this.content;
    }

    /**
     * 设置评论类容
     * 
     * @param content
     *          评论类容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 获取评论时间
     * 
     * @return 评论时间
     */
    public Date getRemarktime() {
        return this.remarktime;
    }

    /**
     * 设置评论时间
     * 
     * @param remarktime
     *          评论时间
     */
    public void setRemarktime(Date remarktime) {
        this.remarktime = remarktime;
    }

    public Remark(Integer id, Integer userid, Integer isshow, String content, Date remarktime) {
        this.id = id;
        this.userid = userid;
        this.isshow = isshow;
        this.content = content;
        this.remarktime = remarktime;
    }

    public Remark() {
    }
}