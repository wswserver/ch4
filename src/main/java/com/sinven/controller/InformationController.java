package com.sinven.controller;

import com.sinven.pojo.Admin;
import com.sinven.pojo.Information;
import com.sinven.service.AdminService;
import com.sinven.service.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class InformationController {
    @Autowired
    InformationService InformationService;
    @RequestMapping("/information")
    public String informationservice(Model model){
        List<Information> all = InformationService.findAll();
        model.addAttribute("informations",all);
        return "/information/information";
    }
}
