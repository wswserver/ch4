package com.sinven.service.impl;

import com.sinven.dao.ShopcartDao;
import com.sinven.pojo.Shopcart;
import com.sinven.service.ShopcartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShopcartServiceImpl implements ShopcartService {
    @Autowired
    ShopcartDao sdao;
    @Override
    public int add(Shopcart c) {
        return sdao.add(c);
    }

    @Override
    public int delete(Integer id) {
        return sdao.delete(id);
    }

    @Override
    public int edit(Shopcart c) {
        return sdao.edit(c);
    }

    @Override
    public Shopcart findById(Integer id) {
        return sdao.findById(id);
    }

    @Override
    public List<Shopcart> findAll() {
        return sdao.findAll();
    }

    @Override
    public int cuscount() {
        return sdao.cuscount();
    }

    @Override
    public List<Shopcart> toPage(int skip, int size) {

        return sdao.toPage((skip-1)*size,size);
    }
}
