package com.sinven.controller;

import com.sinven.pojo.Admin;
import com.sinven.pojo.Product;
import com.sinven.service.AdminService;
import com.sinven.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class ProductController {
    @Autowired
    ProductService productService;
    @RequestMapping("/product")
    public String productservice(Model model){
        List<Product> all = productService.findAll();
        model.addAttribute("products",all);
        return "/product/product";
    }
}
