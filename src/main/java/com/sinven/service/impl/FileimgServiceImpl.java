package com.sinven.service.impl;

import com.sinven.dao.FileimgDao;
import com.sinven.dao.FileimgDao;
import com.sinven.pojo.Fileimg;
import com.sinven.service.FileimgService;
import com.sinven.service.FileimgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileimgServiceImpl implements FileimgService {
    @Autowired
    FileimgDao fdao;
    @Override
    public int add(Fileimg c) {
        return fdao.add(c);
    }

    @Override
    public int delete(Integer id) {
        return fdao.delete(id);
    }

    @Override
    public int edit(Fileimg c) {
        return fdao.edit(c);
    }

    @Override
    public Fileimg findById(Integer id) {
        return fdao.findById(id);
    }

    @Override
    public List<Fileimg> findAll() {
        return fdao.findAll();
    }

    @Override
    public int cuscount() {
        return fdao.cuscount();
    }

    @Override
    public List<Fileimg> toPage(int skip, int size) {

        return fdao.toPage((skip-1)*size,size);
    }
}
