<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2020/7/11
  Time: 16:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>管理员</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap/css/bootstrap.css"/>
    <script type="application/javascript"
            src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.min.js"></script>
</head>
<body style="margin: 0 auto;">
<div class="container"
     style="border: 1px solid #000;align-content: center;background-color: black;color: aliceblue;font-size: 2em;">
    <div class="row">
        <table class="table table-primary table-responsive">
            <tr>
                <td>id</td>
                <td>商品名称</td>
                <td>商品价格</td>
                <td>商品折扣价格</td>
                <td>商品描述</td>
                <td>categoryid</td>
            </tr>
            <c:forEach items="products" var="p">
                <tr>
                    <td>${p.id}</td>
                    <td>${p.name}</td>
                    <td>${p.price}</td>
                    <td>${p.discoprice}</td>
                    <td>${p.description}</td>
                    <td>${p.categoryid}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
</body>
</html>
