package com.sinven.controller;

import com.sinven.pojo.Admin;
import com.sinven.pojo.Orderdetails;
import com.sinven.service.AdminService;
import com.sinven.service.OrderdetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class OrderdetailsController {
    @Autowired
    OrderdetailsService orderdetailsService;
    @RequestMapping("/orderdetails")
    public String orderdetailsservice(Model model){
        List<Orderdetails> all = orderdetailsService.findAll();
        model.addAttribute("orderdetails",all);
        return "/orderdetails/orderdetails";
    }
}
