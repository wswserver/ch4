package com.sinven.controller;

import com.sinven.pojo.Admin;
import com.sinven.pojo.Category;
import com.sinven.service.AdminService;
import com.sinven.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class CategoryController {
    @Autowired
    CategoryService categoryService;
    @RequestMapping("/category")
    public String categoryservice(Model model){
        List<Category> all = categoryService.findAll();
        model.addAttribute("categorys",all);
        return "/category/category";
    }
}
