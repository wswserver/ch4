package com.sinven.dao;

import com.sinven.pojo.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CategoryDao {
    @Insert("insert into category values(null,#{name},#{pid},#{fid})")
    public int add(Category c);

    @Delete("delete from category where id=#{id}")
    public int delete(Integer id);

    @Update("update category set name=#{name},pid=#{pid},fid=#{fid} where id=#{id}")
    public int edit(Category c);

    @Select("select * from category where id=#{id}")
    public Category findById(Integer id);

    @Select("select * from category")
    public List<Category> findAll();

    @Select("select count(*) from category")
    public int cuscount();

    @Select("select * from category order by id desc limit #{skip},#{size}")
    public List<Category> toPage(@Param("skip") int skip, @Param("size") int size);
}
