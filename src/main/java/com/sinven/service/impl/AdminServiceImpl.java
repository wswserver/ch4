package com.sinven.service.impl;

import com.sinven.dao.AdminDao;
import com.sinven.dao.AdminDao;
import com.sinven.pojo.Admin;
import com.sinven.service.AdminService;
import com.sinven.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService{
    @Autowired
    AdminDao adao;
    @Override
    public int add(Admin c) {
        return adao.add(c);
    }

    @Override
    public int delete(Integer id) {
        return adao.delete(id);
    }

    @Override
    public int edit(Admin c) {
        return adao.edit(c);
    }

    @Override
    public Admin findById(Integer id) {
        return adao.findById(id);
    }

    @Override
    public List<Admin> findAll() {
        return adao.findAll();
    }

    @Override
    public int cuscount() {
        return adao.cuscount();
    }

    @Override
    public List<Admin> toPage(int skip, int size) {

        return adao.toPage((skip-1)*size,size);
    }
}
