package com.sinven.dao;

import com.sinven.pojo.Admin;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AdminDao {
    @Insert("insert into admin values(null,#{username},#{pwd},#{role},#{phone})")
    public int add(Admin c);

    @Delete("delete from admin where id=#{id}")
    public int delete(Integer id);

    @Update("update admin set username=#{username},pwd=#{pwd},phone=#{phone},role=#{role} where id=#{id}")
    public int edit(Admin c);

    @Select("select * from admin where id=#{id}")
    public Admin findById(Integer id);

    @Select("select * from admin")
    public List<Admin> findAll();

    @Select("select count(*) from admin")
    public int cuscount();

    @Select("select * from admin order by id desc limit #{skip},#{size}")
    public List<Admin> toPage(@Param("skip") int skip, @Param("size") int size);
}
