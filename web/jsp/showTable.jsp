<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/6/15
  Time: 16:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html class="x-admin-sm">
<head>

    <title>数据表格的练习</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/Xadmin/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/Xadmin/css/xadmin.css">
    <script src="${pageContext.request.contextPath}/static/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/Xadmin/js/xadmin.js"></script>

</head>
<body>
<div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">演示</a>
            <a>
              <cite>导航元素</cite></a>
          </span>
    <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
        <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
</div>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body ">
                    <form class="layui-form layui-col-space5">
                        <div class="layui-inline layui-show-xs-block">
                            <input class="layui-input"  autocomplete="off" placeholder="开始日" name="start" id="start">
                        </div>
                        <div class="layui-inline layui-show-xs-block">
                            <input class="layui-input"  autocomplete="off" placeholder="截止日" name="end" id="end">
                        </div>
                        <div class="layui-inline layui-show-xs-block">
                            <input type="text" name="username"  placeholder="请输入用户名" autocomplete="off" class="layui-input">
                        </div>
                        <div class="layui-inline layui-show-xs-block">
                            <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                        </div>
                    </form>
                </div>
                <div class="layui-card-header">

                    <button class="layui-btn" onclick="xadmin.open('添加用户',
                            '${pageContext.request.contextPath}/customer/addCustomer'
                            ,600,400)"><i class="layui-icon"></i>添加</button>
                </div>
                <div class="layui-card-body ">
                    <table id="table1" class="layui-table layui-form" lay-filter="test">

                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm layui-btn-danger" lay-event="getCheckData">
            <i class="layui-icon"></i>批量删除
        </button>

        <button class="layui-btn layui-btn-sm" lay-event="getCheckLength">获取选中数目</button>
        <button class="layui-btn layui-btn-sm" lay-event="isAll">验证是否全选</button>
    </div>
</script>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script>

    layui.use(['table','laydate','form','jquery'], function(){
        var laydate = layui.laydate;
        $ = layui.jquery;
        var form = layui.form;
        var table=layui.table;
        //执行一个laydate实例
        laydate.render({
            elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
            elem: '#end' //指定元素
        });

        //第一个实例
        table.render({
            elem: '#table1'
            ,toolbar: '#toolbarDemo'
            ,title: '用户'
            ,limit:4
            ,limits:[5,6,7,8,9,10,20,30]
            ,url: '${pageContext.request.contextPath}/customer/cuslistajax' //数据接口
            ,page: true //开启分页
            ,loading:true
            ,cols: [[ //表头
                {type: 'checkbox', fixed: 'left'}
                ,{field: 'id', title: 'ID', width:80, sort: true, fixed: 'left'}
                ,{field: 'username', title: '用户名', width:120, sort: true}
                ,{field: 'jobs', title: '工作', width: 120}
                ,{field: 'phone', title: '电话', width: 180, sort: true}
                ,{fixed: 'right', title:'操作'
                ,toolbar: '#barDemo', width:220}
            ]]
        });
        //监听工具条
        table.on('tool(test)', function(obj){
            var data = obj.data;//把要编辑表格的当前行的数据取出来
            //查看
            if(obj.event === 'detail'){
                xadmin.open('查看顾客','${pageContext.request.contextPath}/major/detailmajor?id='+data.id,800,600);

            } else if(obj.event === 'del'){
                layer.confirm('确定要删除吗？', function(index){
                    $.ajax({
                        url:'${pageContext.request.contextPath}/customer/dodelcustomer',
                        type:'GET',
                        data:{
                            id:data.id
                        },
                        success:function (msg) {
                            if(msg=="\"1\""){
                                layer.msg("删除成功", {icon: 6});
                                setTimeout(function(){
                                    layer.closeAll();//关闭所有的弹出层
                                }, 1000);
                                obj.del();
                                layer.close(index);
                            }else{
                                layer.msg("删除失败:"+msg, {icon: 5});
                            }
                        }
                    });

                });
            } else if(obj.event === 'edit'){
                xadmin.open('编辑顾客','${pageContext.request.contextPath}/customer/editcustomer?id='+data.id,600,400);
            }
        });

        table.on('toolbar(test)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
                case 'getCheckData':
                    var data = checkStatus.data;
                    layer.alert(JSON.stringify(data));
                    layer.confirm('确定要删除吗？', function(index){
                        $.ajax({
                            url:'${pageContext.request.contextPath}/customer/doalldelcustomer',
                            type:'post',
                            contentType: "application/json;charset=UTF-8",
                            data:JSON.stringify(data),
                            success:function (msg) {
                                if(msg=="\"1\""){
                                    var x=$(".layui-form-checked").parent("tr");
                                    alert(x.length);
                                    x.remove();
                                    $(".layui-form-checked").parent("tr").remove();
                                    //layui.alert($(".layui-form-checked").length);
                                    layer.msg("删除成功", {icon: 6});
                                    setTimeout(function(){
                                        layer.closeAll();//关闭所有的弹出层
                                    }, 1000);

                                    layer.close(index);
                                }else{
                                    layer.msg("删除失败:"+msg, {icon: 5});
                                }
                            }
                        });
                    });
                    break;
                case 'getCheckLength':
                    var data = checkStatus.data;
                    layer.msg('选中了：'+ data.length + ' 个');
                    break;
                case 'isAll':
                    layer.msg(checkStatus.isAll ? '全选': '未全选')
                    break;
            };
        });
    });
    function delAll () {

        var data = tableCheck.getData();

        layer.confirm('确认要删除吗？'+data,function(index){
            //捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
    }

</script>

</html>
