package com.sinven.dao;

import com.sinven.pojo.Information;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface InformationDao {
    @Insert("insert into information values(null,#{adminid},#{content},#{infotime})")
    public int add(Information c);

    @Delete("delete from information where id=#{id}")
    public int delete(Integer id);

    @Update("update information set adminid=#{adminid},content=#{content},infotime=#{infotime} where id=#{id}")
    public int edit(Information c);

    @Select("select * from information where id=#{id}")
    public Information findById(Integer id);

    @Select("select * from information")
    public List<Information> findAll();

    @Select("select count(*) from information")
    public int cuscount();

    @Select("select * from information order by id desc limit #{skip},#{size}")
    public List<Information> toPage(@Param("skip") int skip, @Param("size") int size);
}
