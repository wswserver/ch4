package com.sinven.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pager<T> {
    /**
     * 每页显示条数
     */
    Integer pagesize;
    /**
     * 当前页码
     */
    Integer currentPage;
    /**
     * 最大页码
     */
    Integer maxPage;
    /**
     * 分页数据集合
     */
    List<T> lists;
    /**
     * 总条数
     */
    Integer count;
}
