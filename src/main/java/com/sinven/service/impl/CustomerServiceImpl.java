package com.sinven.service.impl;

import com.sinven.dao.CustomerDao;
import com.sinven.pojo.Customer;
import com.sinven.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerDao cdao;
    @Override
    public int add(Customer c) {
        return cdao.add(c);
    }

    @Override
    public int delete(Integer id) {
        return cdao.delete(id);
    }

    @Override
    public int edit(Customer c) {
        return cdao.edit(c);
    }

    @Override
    public Customer findById(Integer id) {
        return cdao.findById(id);
    }

    @Override
    public List<Customer> findAll() {
        return cdao.findAll();
    }

    @Override
    public int cuscount() {
        return cdao.cuscount();
    }

    @Override
    public List<Customer> toPage(int skip, int size) {

        return cdao.toPage((skip-1)*size,size);
    }
}
