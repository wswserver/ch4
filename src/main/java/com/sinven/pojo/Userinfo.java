package com.sinven.pojo;

import java.math.BigDecimal;

/**
 * 用户信息表
 * */
public class Userinfo{
    /** id */
    private Integer id;

    /** 姓名 */
    private String username;

    /** 密码 */
    private String pwd;

    /** 电话 */
    private String phone;

    /** 积分 */
    private Integer jifen;

    /** 地址 */
    private String address;

    /** 金额 */
    private BigDecimal money;


    /**
     * 获取id
     * 
     * @return id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置id
     * 
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取姓名
     * 
     * @return 姓名
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * 设置姓名
     * 
     * @param username
     *          姓名
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取密码
     * 
     * @return 密码
     */
    public String getPwd() {
        return this.pwd;
    }

    /**
     * 设置密码
     * 
     * @param pwd
     *          密码
     */
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    /**
     * 获取电话
     * 
     * @return 电话
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     * 设置电话
     * 
     * @param phone
     *          电话
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取积分
     * 
     * @return 积分
     */
    public Integer getJifen() {
        return this.jifen;
    }

    /**
     * 设置积分
     * 
     * @param jifen
     *          积分
     */
    public void setJifen(Integer jifen) {
        this.jifen = jifen;
    }

    /**
     * 获取地址
     * 
     * @return 地址
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * 设置地址
     * 
     * @param address
     *          地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取金额
     * 
     * @return 金额
     */
    public BigDecimal getMoney() {
        return this.money;
    }

    /**
     * 设置金额
     * 
     * @param money
     *          金额
     */
    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Userinfo(Integer id, String username, String pwd, String phone, Integer jifen, String address, BigDecimal money) {
        this.id = id;
        this.username = username;
        this.pwd = pwd;
        this.phone = phone;
        this.jifen = jifen;
        this.address = address;
        this.money = money;
    }

    public Userinfo() {
    }
}