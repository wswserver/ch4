package com.sinven.service;

import com.sinven.pojo.Product;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductService {

    public int add(Product c);

    public int delete(Integer id);

    public int edit(Product c);

    public Product findById(Integer id);

    public List<Product> findAll();

    public int cuscount();

    public List<Product> toPage(@Param("skip") int skip, @Param("size") int size);
}
