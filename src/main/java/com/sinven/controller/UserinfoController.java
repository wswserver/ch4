package com.sinven.controller;

import com.sinven.pojo.Userinfo;
import com.sinven.service.UserinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class UserinfoController {
    @Autowired
    UserinfoService userinfoService;
    @RequestMapping("/userinfo")
    public String userinfoservice(Model model){
        List<Userinfo> all = userinfoService.findAll();
        model.addAttribute("userinfos",all);
        return "/userinfo/userinfo";
    }
}
