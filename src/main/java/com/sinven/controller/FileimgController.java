package com.sinven.controller;

import com.sinven.pojo.Admin;
import com.sinven.pojo.Fileimg;
import com.sinven.service.AdminService;
import com.sinven.service.FileimgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class FileimgController {
    @Autowired
    FileimgService fileimgService;
    @RequestMapping("/fileimg")
    public String fileimgservice(Model model){
        List<Fileimg> all = fileimgService.findAll();
        model.addAttribute("fileimgs",all);
        return "/fileimg/fileimg";
    }
}
