package com.sinven.service;

import com.sinven.pojo.Admin;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface AdminService {

    public int add(Admin c);

    public int delete(Integer id);

    public int edit(Admin c);

    public Admin findById(Integer id);

    public List<Admin> findAll();

    public int cuscount();

    public List<Admin> toPage(@Param("skip") int skip, @Param("size") int size);
}
