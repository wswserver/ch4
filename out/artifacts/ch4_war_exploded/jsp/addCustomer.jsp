<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/6/17
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title>添加顾客</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/Xadmin/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/Xadmin/css/xadmin.css">
    <script src="${pageContext.request.contextPath}/static/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/Xadmin/js/xadmin.js"></script>

</head>
<body>
<div class="layui-fluid">
    <div class="layui-row">
        <form class="layui-form">
            <div class="layui-form-item">
                <label for="username" class="layui-form-label">
                    <span class="x-red">*</span>顾客名
                </label>
                <div class="layui-input-inline">
                    <input type="text" id="username" name="username" required="" lay-verify="required"
                           autocomplete="off" class="layui-input">
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <span class="x-red">*</span>将会成为您唯一的登入名
                </div>
            </div>
            <div class="layui-form-item">
                <label for="phone" class="layui-form-label">
                    <span class="x-red">*</span>手机
                </label>
                <div class="layui-input-inline">
                    <input type="text" id="phone" name="phone"  lay-verify="phone"
                           autocomplete="off" class="layui-input">
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <span class="x-red">*</span>将会成为您唯一的登入名
                </div>
            </div>
            <div class="layui-form-item">
                <label for="${pageContext.request.contextPath}" class="layui-form-label">
                    <span class="x-red">*</span>工作
                </label>
                <div class="layui-input-inline">
                    <input type="text" id="${pageContext.request.contextPath}" name="jobs"  lay-verify="required"
                           autocomplete="off" class="layui-input">
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <span class="x-red">*</span>
                </div>
            </div>






            <div class="layui-form-item">
                <button type="button" class="layui-btn" id="test1">
                    <i class="layui-icon">&#xe67c;</i>上传图片
                </button>
                <button  class="layui-btn" lay-filter="add" lay-submit="">
                    增加
                </button>
            </div>
        </form>
    </div>
</div>
<script>
    layui.use(['form', 'layer','upload'],
    function() {
        $ = layui.jquery;
        var upload = layui.upload;
        var form = layui.form,
            layer = layui.layer;

        //自定义验证规则
        form.verify({
            nikename: function(value) {
                if (value.length < 5) {
                    return '昵称至少得5个字符啊';
                }
            },
            pass: [/(.+){6,12}$/, '密码必须6到12位'],
            repass: function(value) {
                if ($('#L_pass').val() != $('#L_repass').val()) {
                    return '两次密码不一致';
                }
            }
        });

        //监听提交
        form.on('submit(add)',
            function(data) {
                console.log(JSON.stringify(data.field));
                //发异步，把数据提交给php
                $.ajax({
                    url: '${pageContext.request.contextPath}/customer/doaddCustomer',
                    type: 'post',
                    contentType: 'application/json',
                    data: JSON.stringify(data.field),
                    success: function (msg) {
                        //alert(msg);
                        if (msg=="\"添加成功\"") {
                            layer.alert("增加成功", {
                                    icon: 6
                                },
                                function () {

                                    //关闭当前frame
                                    xadmin.close();
                                    // 可以对父窗口进行刷新
                                    xadmin.father_reload();
                                });
                        } else {
                            layer.msg(msg, {icon: 5});
                        }
                    }, error: function () {
                        layer.msg("添加失败，出现错误！",{icon:1,time:1000});
                    }
                });
                return false;
            });
        //文件上传
        var uploadInst = upload.render({
            elem: '#test1' //绑定元素
            ,url: '${pageContext.request.contextPath}/customer/doupload' //上传接口
            ,done: function(res){
                //上传完毕回调
                alert("上传成功！");
            }
            ,error: function(){
                //请求异常回调
                alert("上传失败！");
            }
        });

    });</script>

</body>

</html>
