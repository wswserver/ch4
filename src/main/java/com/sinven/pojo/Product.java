package com.sinven.pojo;

import java.math.BigDecimal;

/**
 * 产品
 * */
public class Product{
    /** id */
    private Integer id;

    /** 商品名称 */
    private String name;

    /** 商品价格 */
    private BigDecimal price;

    /** 商品折扣价格 */
    private BigDecimal discoprice;

    /** 商品描述 */
    private String description;

    /** categoryid */
    private Integer categoryid;


    /**
     * 获取id
     * 
     * @return id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置id
     * 
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取商品名称
     * 
     * @return 商品名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置商品名称
     * 
     * @param name
     *          商品名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取商品价格
     * 
     * @return 商品价格
     */
    public BigDecimal getPrice() {
        return this.price;
    }

    /**
     * 设置商品价格
     * 
     * @param price
     *          商品价格
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * 获取商品折扣价格
     * 
     * @return 商品折扣价格
     */
    public BigDecimal getDiscoprice() {
        return this.discoprice;
    }

    /**
     * 设置商品折扣价格
     * 
     * @param discoprice
     *          商品折扣价格
     */
    public void setDiscoprice(BigDecimal discoprice) {
        this.discoprice = discoprice;
    }

    /**
     * 获取商品描述
     * 
     * @return 商品描述
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * 设置商品描述
     * 
     * @param description
     *          商品描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取categoryid
     * 
     * @return categoryid
     */
    public Integer getCategoryid() {
        return this.categoryid;
    }

    /**
     * 设置categoryid
     * 
     * @param categoryid
     */
    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public Product(Integer id, String name, BigDecimal price, BigDecimal discoprice, String description, Integer categoryid) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.discoprice = discoprice;
        this.description = description;
        this.categoryid = categoryid;
    }

    public Product() {
    }
}