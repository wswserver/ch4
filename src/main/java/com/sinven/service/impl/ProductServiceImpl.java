package com.sinven.service.impl;

import com.sinven.dao.ProductDao;
import com.sinven.pojo.Product;
import com.sinven.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductDao pdao;
    @Override
    public int add(Product c) {
        return pdao.add(c);
    }

    @Override
    public int delete(Integer id) {
        return pdao.delete(id);
    }

    @Override
    public int edit(Product c) {
        return pdao.edit(c);
    }

    @Override
    public Product findById(Integer id) {
        return pdao.findById(id);
    }

    @Override
    public List<Product> findAll() {
        return pdao.findAll();
    }

    @Override
    public int cuscount() {
        return pdao.cuscount();
    }

    @Override
    public List<Product> toPage(int skip, int size) {

        return pdao.toPage((skip-1)*size,size);
    }
}
