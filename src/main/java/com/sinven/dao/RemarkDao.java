package com.sinven.dao;

import com.sinven.pojo.Remark;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface RemarkDao {
    @Insert("insert into remark values(null,#{userid},#{isshow},#{content},#{remarktime})")
    public int add(Remark c);

    @Delete("delete from remark where id=#{id}")
    public int delete(Integer id);

    @Update("update remark set userid=#{userid},isshow=#{isshow},content=#{content},remarktime=#{remarktime} where id=#{id}")
    public int edit(Remark c);

    @Select("select * from remark where id=#{id}")
    public Remark findById(Integer id);

    @Select("select * from remark")
    public List<Remark> findAll();

    @Select("select count(*) from remark")
    public int cuscount();

    @Select("select * from remark order by id desc limit #{skip},#{size}")
    public List<Remark> toPage(@Param("skip") int skip, @Param("size") int size);
}
