package com.sinven.service.impl;

import com.sinven.dao.InformationDao;
import com.sinven.pojo.Information;
import com.sinven.service.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InformationServiceImpl implements InformationService {
    @Autowired
    InformationDao idao;
    @Override
    public int add(Information c) {
        return idao.add(c);
    }

    @Override
    public int delete(Integer id) {
        return idao.delete(id);
    }

    @Override
    public int edit(Information c) {
        return idao.edit(c);
    }

    @Override
    public Information findById(Integer id) {
        return idao.findById(id);
    }

    @Override
    public List<Information> findAll() {
        return idao.findAll();
    }

    @Override
    public int cuscount() {
        return idao.cuscount();
    }

    @Override
    public List<Information> toPage(int skip, int size) {

        return idao.toPage((skip-1)*size,size);
    }
}
