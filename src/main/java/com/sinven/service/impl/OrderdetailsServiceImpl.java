package com.sinven.service.impl;

import com.sinven.dao.OrderdetailsDao;
import com.sinven.pojo.Orderdetails;
import com.sinven.service.OrderdetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderdetailsServiceImpl implements OrderdetailsService {
    @Autowired
    OrderdetailsDao odao;
    @Override
    public int add(Orderdetails c) {
        return odao.add(c);
    }

    @Override
    public int delete(Integer id) {
        return odao.delete(id);
    }

    @Override
    public int edit(Orderdetails c) {
        return odao.edit(c);
    }

    @Override
    public Orderdetails findById(Integer id) {
        return odao.findById(id);
    }

    @Override
    public List<Orderdetails> findAll() {
        return odao.findAll();
    }

    @Override
    public int cuscount() {
        return odao.cuscount();
    }

    @Override
    public List<Orderdetails> toPage(int skip, int size) {

        return odao.toPage((skip-1)*size,size);
    }
}
