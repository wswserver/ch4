package com.sinven.controller;

import com.sinven.pojo.Admin;
import com.sinven.pojo.Orders;
import com.sinven.service.AdminService;
import com.sinven.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class OrdersController {
    @Autowired
    OrdersService ordersService;
    @RequestMapping("/orders")
    public String ordersservice(Model model){
        List<Orders> all = ordersService.findAll();
        model.addAttribute("orders",all);
        return "/orders/orders";
    }
}
