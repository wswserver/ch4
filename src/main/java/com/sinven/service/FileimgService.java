package com.sinven.service;

import com.sinven.pojo.Fileimg;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FileimgService {

    public int add(Fileimg c);

    public int delete(Integer id);

    public int edit(Fileimg c);

    public Fileimg findById(Integer id);

    public List<Fileimg> findAll();

    public int cuscount();

    public List<Fileimg> toPage(@Param("skip") int skip, @Param("size") int size);
}
