package com.sinven.dao;

import com.sinven.pojo.Userinfo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserinfoDao {
    @Insert("insert into userinfo values(null,#{username},#{pwd},#{phone},#{jifen},#{address},#{money})")
    public int add(Userinfo c);

    @Delete("delete from userinfo where id=#{id}")
    public int delete(Integer id);

    @Update("update userinfo set username=#{username},pwd=#{pwd},phone=#{phone},jifen=#{jifen},address=#{address},money=#{money} where id=#{id}")
    public int edit(Userinfo c);

    @Select("select * from userinfo where id=#{id}")
    public Userinfo findById(Integer id);

    @Select("select * from userinfo")
    public List<Userinfo> findAll();

    @Select("select count(*) from userinfo")
    public int cuscount();

    @Select("select * from userinfo order by id desc limit #{skip},#{size}")
    public List<Userinfo> toPage(@Param("skip") int skip, @Param("size") int size);
}
