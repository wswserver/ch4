<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/6/17
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title>修改顾客</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/Xadmin/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/Xadmin/css/xadmin.css">
    <script src="${pageContext.request.contextPath}/static/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/Xadmin/js/xadmin.js"></script>

</head>
<body>
<div class="layui-fluid">
    <div class="layui-row">
        <form class="layui-form">
            <div class="layui-form-item">
                <label for="id" class="layui-form-label">
                    <span class="x-red">*</span>id
                </label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" id="id" name="id" readonly="readonly" value="${cus.id}">
                </div>

            </div>
            <div class="layui-form-item">
                <label for="username" class="layui-form-label">
                    <span class="x-red">*</span>顾客名
                </label>
                <div class="layui-input-inline">
                    <input type="text" id="username" name="username" required="" lay-verify="required"
                           autocomplete="off" class="layui-input" value="${cus.username}">
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <span class="x-red">*</span>顾客名称
                </div>
            </div>
            <div class="layui-form-item">
                <label for="phone" class="layui-form-label">
                    <span class="x-red">*</span>手机
                </label>
                <div class="layui-input-inline">
                    <input type="text" id="phone" name="phone"  lay-verify="phone"
                           autocomplete="off" class="layui-input" value="${cus.phone}">
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <span class="x-red">*</span>将会成为您唯一的登入名
                </div>
            </div>
            <div class="layui-form-item">
                <label for="${pageContext.request.contextPath}" class="layui-form-label">
                    <span class="x-red">*</span>工作
                </label>
                <div class="layui-input-inline">
                    <input type="text" id="${pageContext.request.contextPath}" name="jobs"  lay-verify="required"
                           autocomplete="off" class="layui-input" value="${cus.jobs}">
                </div>
                <div class="layui-form-mid layui-word-aux">
                    <span class="x-red">*</span>
                </div>
            </div>






            <div class="layui-form-item">

                <button  class="layui-btn" lay-filter="edit" lay-submit="">
                    修改
                </button>
            </div>
        </form>
    </div>
</div>
<script>
    layui.use(['form', 'layer'],
    function() {
        $ = layui.jquery;
        var form = layui.form,
            layer = layui.layer;

        //自定义验证规则
        form.verify({
            nikename: function(value) {
                if (value.length < 5) {
                    return '昵称至少得5个字符啊';
                }
            },
            pass: [/(.+){6,12}$/, '密码必须6到12位'],
            repass: function(value) {
                if ($('#L_pass').val() != $('#L_repass').val()) {
                    return '两次密码不一致';
                }
            }
        });

        //监听提交
        form.on('submit(edit)',
            function(data) {
                console.log(JSON.stringify(data.field));
                //发异步，把数据提交给php
                $.ajax({
                    url: '${pageContext.request.contextPath}/customer/doeditcustomer',
                    type: 'post',
                    contentType: 'application/json',
                    data: JSON.stringify(data.field),
                    success: function (msg) {
                        //alert(msg);
                        if (msg=="\"修改成功\"") {
                            layer.alert("修改成功", {
                                    icon: 6
                                },
                                function () {
                                    //关闭当前frame
                                    xadmin.close();
                                    // 可以对父窗口进行刷新
                                    xadmin.father_reload();
                                });
                        } else {
                            layer.msg(msg, {icon: 5});
                        }
                    }, error: function () {
                        layer.msg("修改失败，出现错误！",{icon:1,time:1000});
                    }
                });
                return false;
            });

    });</script>

</body>

</html>
