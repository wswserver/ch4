package com.sinven.dao;

import com.sinven.pojo.Customer;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CustomerDao {
    @Insert("insert into t_customer values(null,#{username},#{jobs},#{phone})")
    public int add(Customer c);

    @Delete("delete from t_customer where id=#{id}")
    public int delete(Integer id);

    @Update("update t_customer set username=#{username},jobs=#{jobs},phone=#{phone} where id=#{id}")
    public int edit(Customer c);

    @Select("select * from t_customer where id=#{id}")
    public Customer findById(Integer id);

    @Select("select * from t_customer")
    public List<Customer> findAll();

    @Select("select count(*) from t_customer")
    public int cuscount();

    @Select("select * from t_customer order by id desc limit #{skip},#{size}")
    public List<Customer> toPage(@Param("skip") int skip, @Param("size") int size);
}
