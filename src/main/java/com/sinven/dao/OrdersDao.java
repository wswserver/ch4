package com.sinven.dao;

import com.sinven.pojo.Orders;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface OrdersDao {
    @Insert("insert into orders values(#{id},#{ordertime},#{orderstatus},#{userid},#{totalprice})")
    public int add(Orders c);

    @Delete("delete from orders where id=#{id}")
    public int delete(String id);

    @Update("update orders set ordertime=#{ordertime},orderstatus=#{orderstatus},userid=#{userid},totalprice=#{totalprice} where id=#{id}")
    public int edit(Orders c);

    @Select("select * from orders where id=#{id}")
    public Orders findById(String id);

    @Select("select * from orders")
    public List<Orders> findAll();

    @Select("select count(*) from orders")
    public int cuscount();

    @Select("select * from orders order by id desc limit #{skip},#{size}")
    public List<Orders> toPage(@Param("skip") int skip, @Param("size") int size);
}
