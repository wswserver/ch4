package com.sinven.service;

import com.sinven.pojo.Customer;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface CustomerService {

    public int add(Customer c);

    public int delete(Integer id);

    public int edit(Customer c);

    public Customer findById(Integer id);

    public List<Customer> findAll();

    public int cuscount();

    public List<Customer> toPage(@Param("skip") int skip, @Param("size") int size);
}
