package com.sinven.service;

import com.sinven.pojo.Remark;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RemarkService {

    public int add(Remark c);

    public int delete(Integer id);

    public int edit(Remark c);

    public Remark findById(Integer id);

    public List<Remark> findAll();

    public int cuscount();

    public List<Remark> toPage(@Param("skip") int skip, @Param("size") int size);
}
