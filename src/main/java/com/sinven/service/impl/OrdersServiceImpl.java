package com.sinven.service.impl;

import com.sinven.dao.OrdersDao;
import com.sinven.pojo.Orders;
import com.sinven.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersServiceImpl implements OrdersService {
    @Autowired
    OrdersDao odao;
    @Override
    public int add(Orders c) {
        return odao.add(c);
    }

    @Override
    public int delete(String id) {
        return odao.delete(id);
    }

    @Override
    public int edit(Orders c) {
        return odao.edit(c);
    }

    @Override
    public Orders findById(String id) {
        return odao.findById(id);
    }

    @Override
    public List<Orders> findAll() {
        return odao.findAll();
    }

    @Override
    public int cuscount() {
        return odao.cuscount();
    }

    @Override
    public List<Orders> toPage(int skip, int size) {

        return odao.toPage((skip-1)*size,size);
    }
}
