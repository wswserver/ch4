package com.sinven.dao;

import com.sinven.pojo.Fileimg;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface FileimgDao {
    @Insert("insert into fileimg values(null,#{filename},#{filesize},#{filetype}#{suffix},#{path},#{uploadtime},#{userid},)")
    public int add(Fileimg c);

    @Delete("delete from fileimg where id=#{id}")
    public int delete(Integer id);

    @Update("update fileimg set filename=#{filename},filesize=#{filesize},filetype=#{filetype},suffix=#{suffix},path=#{path},uploadtime=#{uploadtime},userid=#{userid} where id=#{id}")
    public int edit(Fileimg c);

    @Select("select * from fileimg where id=#{id}")
    public Fileimg findById(Integer id);

    @Select("select * from fileimg")
    public List<Fileimg> findAll();

    @Select("select count(*) from fileimg")
    public int cuscount();

    @Select("select * from fileimg order by id desc limit #{skip},#{size}")
    public List<Fileimg> toPage(@Param("skip") int skip, @Param("size") int size);
}
