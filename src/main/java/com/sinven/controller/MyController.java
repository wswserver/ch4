package com.sinven.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyController {
    @RequestMapping(value = "helloworld")
    public String hello(){
        return "helloworld";
    }
}
