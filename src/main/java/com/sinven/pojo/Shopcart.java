package com.sinven.pojo;


public class Shopcart{
    /** id */
    private Integer id;

    /** 所买商品 */
    private Integer productid;

    /** 商品数量 */
    private Integer mycount;

    /** 购买人 */
    private Integer userid;


    /**
     * 获取id
     * 
     * @return id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置id
     * 
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取所买商品
     * 
     * @return 所买商品
     */
    public Integer getProductid() {
        return this.productid;
    }

    /**
     * 设置所买商品
     * 
     * @param productid
     *          所买商品
     */
    public void setProductid(Integer productid) {
        this.productid = productid;
    }

    /**
     * 获取商品数量
     * 
     * @return 商品数量
     */
    public Integer getMycount() {
        return this.mycount;
    }

    /**
     * 设置商品数量
     * 
     * @param mycount
     *          商品数量
     */
    public void setMycount(Integer mycount) {
        this.mycount = mycount;
    }

    /**
     * 获取购买人
     * 
     * @return 购买人
     */
    public Integer getUserid() {
        return this.userid;
    }

    /**
     * 设置购买人
     * 
     * @param userid
     *          购买人
     */
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Shopcart(Integer id, Integer productid, Integer mycount, Integer userid) {
        this.id = id;
        this.productid = productid;
        this.mycount = mycount;
        this.userid = userid;
    }

    public Shopcart() {
    }
}