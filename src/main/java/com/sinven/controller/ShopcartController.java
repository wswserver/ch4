package com.sinven.controller;

import com.sinven.pojo.Admin;
import com.sinven.pojo.Shopcart;
import com.sinven.service.AdminService;
import com.sinven.service.ShopcartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class ShopcartController {
    @Autowired
    ShopcartService shopcartService;
    @RequestMapping("/shopcart")
    public String shopcartservice(Model model){
        List<Shopcart> all = shopcartService.findAll();
        model.addAttribute("shopcarts",all);
        return "/shopcart/shopcart";
    }
}
