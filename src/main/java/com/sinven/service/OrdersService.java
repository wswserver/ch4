package com.sinven.service;

import com.sinven.pojo.Orders;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrdersService {

    public int add(Orders c);

    public int delete(String id);

    public int edit(Orders c);

    public Orders findById(String id);

    public List<Orders> findAll();

    public int cuscount();

    public List<Orders> toPage(@Param("skip") int skip, @Param("size") int size);
}
