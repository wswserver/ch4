<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2020/7/11
  Time: 16:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>管理员</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap/css/bootstrap.css"/>
    <script type="application/javascript"
            src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.min.js"></script>
</head>
<body style="margin: 0 auto;">
<div class="container" style="border: 1px solid #000;align-content: center;background-color: black;color: aliceblue;font-size: 2em;">
    <div class="row">
        <table class="table table-primary table-responsive">
            <tr>
                <td>编号</td>
                <td>管理员 </td>
                <td>公告类容</td>
                <td>公告时间</td>
            </tr>
            <c:forEach items="informations" var="i">
            <tr>
                    <td>${i.id}</td>
                    <td>${i.adminid}</td>
                    <td>${i.content}</td>
                    <td>${i.infotime}</td>
            </tr>
            </c:forEach>
        </table>
    </div>
</div>
</body>
</html>
