package com.sinven.service;

import com.sinven.pojo.Category;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategoryService {

    public int add(Category c);

    public int delete(Integer id);

    public int edit(Category c);

    public Category findById(Integer id);

    public List<Category> findAll();

    public int cuscount();

    public List<Category> toPage(@Param("skip") int skip, @Param("size") int size);
}
