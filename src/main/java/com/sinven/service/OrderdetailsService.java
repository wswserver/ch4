package com.sinven.service;

import com.sinven.pojo.Orderdetails;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderdetailsService {

    public int add(Orderdetails c);

    public int delete(Integer id);

    public int edit(Orderdetails c);

    public Orderdetails findById(Integer id);

    public List<Orderdetails> findAll();

    public int cuscount();

    public List<Orderdetails> toPage(@Param("skip") int skip, @Param("size") int size);
}
