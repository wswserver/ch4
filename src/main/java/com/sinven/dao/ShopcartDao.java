package com.sinven.dao;

import com.sinven.pojo.Shopcart;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ShopcartDao {
    @Insert("insert into shopcart values(null,#{userid},#{isshow},#{content},#{remarktime})")
    public int add(Shopcart c);

    @Delete("delete from shopcart where id=#{id}")
    public int delete(Integer id);

    @Update("update shopcart set userid=#{userid},isshow=#{isshow},content=#{content},remarktime=#{remarktime} where id=#{id}")
    public int edit(Shopcart c);

    @Select("select * from shopcart where id=#{id}")
    public Shopcart findById(Integer id);

    @Select("select * from shopcart")
    public List<Shopcart> findAll();

    @Select("select count(*) from shopcart")
    public int cuscount();

    @Select("select * from shopcart order by id desc limit #{skip},#{size}")
    public List<Shopcart> toPage(@Param("skip") int skip, @Param("size") int size);
}
