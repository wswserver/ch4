package com.sinven.pojo;

import java.util.Date;


public class Fileimg{
    /** id */
    private Integer id;

    /** 文件名称 */
    private String filename;

    /** 文件大小 */
    private Long filesize;

    /** 上传的文件类型图片 */
    private String filetype;

    /** 文件后缀 */
    private String suffix;

    /** 文件路径 */
    private String path;

    /** 上传时间 */
    private Date uploadtime;

    /** 上传人 */
    private Integer userid;

    /**
     * 获取id
     * 
     * @return id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置id
     * 
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取文件名称
     * 
     * @return 文件名称
     */
    public String getFilename() {
        return this.filename;
    }

    /**
     * 设置文件名称
     * 
     * @param filename
     *          文件名称
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * 获取文件大小
     * 
     * @return 文件大小
     */
    public Long getFilesize() {
        return this.filesize;
    }

    /**
     * 设置文件大小
     * 
     * @param filesize
     *          文件大小
     */
    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    /**
     * 获取上传的文件类型图片
     * 
     * @return 上传的文件类型图片
     */
    public String getFiletype() {
        return this.filetype;
    }

    /**
     * 设置上传的文件类型图片
     * 
     * @param filetype
     *          上传的文件类型图片
     */
    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    /**
     * 获取文件后缀
     * 
     * @return 文件后缀
     */
    public String getSuffix() {
        return this.suffix;
    }

    /**
     * 设置文件后缀
     * 
     * @param suffix
     *          文件后缀
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    /**
     * 获取文件路径
     * 
     * @return 文件路径
     */
    public String getPath() {
        return this.path;
    }

    /**
     * 设置文件路径
     * 
     * @param path
     *          文件路径
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * 获取上传时间
     * 
     * @return 上传时间
     */
    public Date getUploadtime() {
        return this.uploadtime;
    }

    /**
     * 设置上传时间
     * 
     * @param uploadtime
     *          上传时间
     */
    public void setUploadtime(Date uploadtime) {
        this.uploadtime = uploadtime;
    }

    /**
     * 获取上传人
     * 
     * @return 上传人
     */
    public Integer getUserid() {
        return this.userid;
    }

    /**
     * 设置上传人
     * 
     * @param userid
     *          上传人
     */
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Fileimg(Integer id, String filename, Long filesize, String filetype, String suffix, String path, Date uploadtime, Integer userid) {
        this.id = id;
        this.filename = filename;
        this.filesize = filesize;
        this.filetype = filetype;
        this.suffix = suffix;
        this.path = path;
        this.uploadtime = uploadtime;
        this.userid = userid;
    }

    public Fileimg() {
    }
}