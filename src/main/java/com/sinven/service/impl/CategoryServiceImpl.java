package com.sinven.service.impl;

import com.sinven.dao.CategoryDao;
import com.sinven.dao.CategoryDao;
import com.sinven.pojo.Category;
import com.sinven.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryDao cdao;
    @Override
    public int add(Category c) {
        return cdao.add(c);
    }

    @Override
    public int delete(Integer id) {
        return cdao.delete(id);
    }

    @Override
    public int edit(Category c) {
        return cdao.edit(c);
    }

    @Override
    public Category findById(Integer id) {
        return cdao.findById(id);
    }

    @Override
    public List<Category> findAll() {
        return cdao.findAll();
    }

    @Override
    public int cuscount() {
        return cdao.cuscount();
    }

    @Override
    public List<Category> toPage(int skip, int size) {

        return cdao.toPage((skip-1)*size,size);
    }
}
