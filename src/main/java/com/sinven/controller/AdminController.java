package com.sinven.controller;

import com.sinven.pojo.Admin;
import com.sinven.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class AdminController {
    @Autowired
    AdminService adminService;
    @RequestMapping("/admin")
    public String adminservice(Model model){
        Integer id=1;
        Admin admin = adminService.findById(id);
        model.addAttribute("admins",admin);

        return "/admin/admin";
    }
}
